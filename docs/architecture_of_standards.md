# Architecture of Standards

The Architecture of Standards (AoS) document defines a target for Gaia-X by analysing and integrating already existing standards for data, sovereignty and infrastructure components and specifying which standards are supported.

## alias
- AoS

## references
- This definition was consolidated from Gaia-X documents
