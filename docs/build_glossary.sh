#!/bin/sh

curdir=`dirname $0`
echo $curdir

glossary=$curdir/../glossary.md

echo "# Glossary" > $glossary

for f in $curdir/*.md; do
    echo $f
    echo "" >> $glossary
    echo "" >> $glossary
    echo "---" >> $glossary
    echo "" >> $glossary
    cat $f >> $glossary
done
