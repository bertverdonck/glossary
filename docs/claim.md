# Claim

An assertion made about a subject within Gaia-X.

## references

<https://www.w3.org/TR/vc-use-cases/#terminology>
