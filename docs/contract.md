# Contract

Contract represents the binding legal agreement describing a [Service Instance](#service-instance) and includes all rights and obligations.
