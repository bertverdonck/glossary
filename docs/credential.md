# Credential

A set of one or more [Claims](#claim) made and asserted by an issuer.

## references
<https://www.w3.org/TR/vc-use-cases/#terminology>
