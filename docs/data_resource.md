# Data Resource

Data Resource is a subclass of [Resource](#resource) and consists of data (also including [derived data](https://www.iso.org/obp/ui/#iso:std:iso-iec:22123:-1:ed-1:v1:en:term:3.10.2)) in any form and includes the necessary information for data sharing.
