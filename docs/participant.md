# Participant

A Participant is an [entity](https://www.iso.org/obp/ui/#iso:std:iso-iec:24760:-1:ed-2:v1:en:term:3.1.1) which is identified, onboarded and has a Gaia-X Self-Description.

A Participant can take on one or multiple of the following roles: [Provider](#provider), [Consumer](#consumer), [Federator](#federator).
