# Principal

A Principal is either a natural person or a digital representation which acts on behalf of a Gaia-X [Participant](#participant).
