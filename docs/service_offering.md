# Service Offering

A Service Offering is a set of [Resources](#resource), which a [Provider](#provider) bundles into an offering.

A Service Offering can be nested with one or more other Service Offerings.
