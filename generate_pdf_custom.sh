#!/bin/sh
# set -ex

# I. merge all md in docs in temp.md
mdArray=docs/*.md

for file in ${mdArray[@]}; do
  if [[ $file = "docs/README.md" ]]; then
  continue
  fi

#   echo $file
  cat $file >> docs/temp.md
done


# II. Enumerate headings in temp.md
markdown-enum docs/temp.md 1 docs/temp.md


# III. Create Table of Content and glossary.md. Merge ToC and temp.md in glossary.md.
toc=$(bash create_TOC.sh docs/temp.md)
echo "$toc" >> docs/glossary.md
echo " " >> docs/glossary.md
cat docs/temp.md >> docs/glossary.md 


# IV. mkdocs build
mkdocs build


# V. Convert glossary.html into glossary.pdf
weasyprint public/glossary/index.html pdf/Gaia-x_Glossary.pdf


